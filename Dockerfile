FROM fluent/fluentd:v1.16-1

# Use root account to use apk
USER root

COPY global-bundle.pem /tmp/global-bundle.pem
RUN cat /tmp/global-bundle.pem >> /etc/ssl/cert.pem

# below RUN includes plugin as examples elasticsearch is not required
# you may customize including plugins as you wish
RUN apk add --no-cache --update --virtual .build-deps \
        sudo build-base ruby-dev \
 && apk add geoip-dev libmaxminddb-dev \
 && sudo gem install fluent-plugin-opensearch \
 && sudo gem install fluent-plugin-s3 \
 && sudo gem install nokogiri \
 && sudo gem install fluent-plugin-prometheus \
 && sudo gem install fluent-plugin-http-healthcheck \
 && sudo gem install fluent-plugin-sanitizer \
 && sudo gem install fluent-plugin-record-modifier \
 && sudo gem install fluent-plugin-rewrite-tag-filter \
 && sudo gem install fluent-plugin-mongo \
 && sudo gem sources --clear-all \
 && apk del .build-deps \
 && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

 ADD locations.json /tmp/CMACGM/locations.json

USER fluent
